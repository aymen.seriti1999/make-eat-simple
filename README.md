# Make Eat Simple

Le projet est destiné à simplifier le suivi alimentaire de ses utilisateurs. L'application vous renseigne sur l'intégralité des valeurs nutritionnelles de tous les aliments que vous souhaitez !